﻿using System;

namespace SQLSingleton.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class Table : Attribute
    {
        public string TableName { get; set; }

        public Table() { }
        public Table(string tableName)
        {
            TableName = tableName;
        }
    }
}
