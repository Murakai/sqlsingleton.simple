﻿using SQLSingleton.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SQLSingleton
{
    [Table("")]
    public class DataBaseObject
    {
        public List<string> ChangedProperties { get; private set; } = new List<string>();
        public bool IsNew { get; private set; }

        [Field("GUID")]
        public Guid Guid { get; set; }

        public DataBaseObject(bool isNew = false)
        {
            if (IsNew = isNew)
                Guid = Guid.NewGuid();
        }
        public DataBaseObject(Guid guid)
        {
            if (guid == Guid.Empty)
                return;
            Guid = guid;
            if (!Load())
                throw new Exception("Failed to load a object of type '" + GetType() + "' with guid '" + Guid + "'!");
        }

        internal void SetFromDataRow(System.Data.DataRow row, List<string> columns)
        {
            var props = GetType().GetProperties().Where(x => Attribute.IsDefined(x, typeof(Field)));
            //set properties from result
            foreach (var prop in props)
            {
                Field field = (Field)prop.GetCustomAttributes(true).First(x => x.GetType() == typeof(Field));
                if (columns.Contains(field.FieldName))
                {
                    if (typeof(DataBaseObject).IsAssignableFrom(prop.PropertyType))
                    {
                        System.Reflection.ConstructorInfo ctor = prop.PropertyType.GetConstructor(new[] { typeof(Guid) });
                        if (ctor != null)
                            prop.SetValue(this, ctor.Invoke(new object[] { (Guid)row[field.FieldName] }));
                    }
                    else
                    {
                        if (field.IsEncrypted)
                            prop.SetValue(this, row[field.FieldName].ToString().Decrypt());
                        else
                            prop.SetValue(this, row[field.FieldName]);
                    }
                }
            }

            ChangedProperties = new List<string>();
        }

        public bool Save()
        {
            if (!Attribute.IsDefined(GetType(), typeof(Table)) || Guid == Guid.Empty)
                return false;

            var table = (Table)GetType().GetCustomAttributes(true).FirstOrDefault(x => x.GetType() == typeof(Table));

            if (table == null || string.IsNullOrWhiteSpace(table.TableName))
                return false;

            var tableName = table.TableName;

            var values = new List<NameValuePair>();
            NameValuePair guidPair = null;

            ChangedProperties = ChangedProperties.Distinct().ToList();

            //get values for update/insert
            var props = GetType().GetProperties().Where(x => Attribute.IsDefined(x, typeof(Field)));
            foreach (var prop in props)
            {
                Field field = (Field)prop.GetCustomAttributes(true).First(x => x.GetType() == typeof(Field));

                var value = !typeof(DataBaseObject).IsAssignableFrom(prop.PropertyType) ? prop.GetValue(this) :
                    (prop.GetValue(this) == null ? Guid.Empty : ((DataBaseObject)prop.GetValue(this)).Guid);

                if (!IsNew)
                {
                    if (field.FieldName == "GUID")
                        guidPair = new NameValuePair(field.FieldName, value);
                    else if (ChangedProperties.Contains(prop.Name))
                        values.Add(new NameValuePair(field.FieldName, value));
                }
                else
                    values.Add(new NameValuePair(field.FieldName, value));

            }

            ChangedProperties = new List<string>();

            if (IsNew)
                return 1 == SQL.Instance.Insert(tableName, values.ToArray());
            else
                return 1 == SQL.Instance.Update(tableName, values, guidPair);
        }

        public bool Load()
        {
            if (!Attribute.IsDefined(GetType(), typeof(Table)))
                return false;

            var table = (Table)GetType().GetCustomAttributes(true).FirstOrDefault(x => x.GetType() == typeof(Table));

            if (table == null || string.IsNullOrWhiteSpace(table.TableName))
                return false;

            var tableName = table.TableName;

            var props = GetType().GetProperties().Where(x => Attribute.IsDefined(x, typeof(Field)));

            var fields = props.Select(x => ((Field)x.GetCustomAttributes(true).First(y => y.GetType() == typeof(Field) && !((Field)y).LoadLater)).FieldName);

            var ds = SQL.Instance.Select(tableName, new NameValuePair("GUID", Guid), fields.ToArray());
            if (ds == null || ds.Tables[0].Rows.Count == 0)
                return false;

            var columns = new List<string>();
            foreach (System.Data.DataColumn column in ds.Tables[0].Columns)
                columns.Add(column.ColumnName);

            SetFromDataRow(ds.Tables[0].Rows[0], columns);

            return true;
        }

        public bool Delete()
        {
            if (!Attribute.IsDefined(GetType(), typeof(Table)))
                return false;

            var table = (Table)GetType().GetCustomAttributes(true).FirstOrDefault(x => x.GetType() == typeof(Table));

            if (table == null || string.IsNullOrWhiteSpace(table.TableName))
                return false;

            var tableName = table.TableName;

            return SQL.Instance.Delete(tableName, new NameValuePair("GUID", Guid)) == 1;
        }
    }

    public class DataBaseObjectCollection<T> : List<T> where T : DataBaseObject
    {
        public DataBaseObjectCollection() : this(Global.Conjunction.And) { }

        public DataBaseObjectCollection(NameValuePair where) : this(Global.Conjunction.And, new[] { where }) { }

        public DataBaseObjectCollection(Global.Conjunction conjunction, params NameValuePair[] where) : base()
        {
            if (!Attribute.IsDefined(typeof(T), typeof(Table)))
                return;

            var table = (Table)typeof(T).GetCustomAttributes(true).FirstOrDefault(x => x.GetType() == typeof(Table));

            if (table == null || string.IsNullOrWhiteSpace(table.TableName))
                return;

            var tableName = table.TableName;

            var props = typeof(T).GetProperties().Where(x => Attribute.IsDefined(x, typeof(Field)));

            var fields = props.Select(x => ((Field)x.GetCustomAttributes(true).First(y => y.GetType() == typeof(Field) && !((Field)y).LoadLater)).FieldName);

            var ds = SQL.Instance.Select(tableName, fields.ToList(), conjunction, where);
            if (ds == null || ds.Tables[0].Rows.Count == 0)
                return;

            var columns = new List<string>();
            foreach (System.Data.DataColumn column in ds.Tables[0].Columns)
                columns.Add(column.ColumnName);

            foreach (System.Data.DataRow row in ds.Tables[0].Rows)
            {
                T obj = (T)Activator.CreateInstance(typeof(T));
                obj.SetFromDataRow(row, columns);
                Add(obj);
            }
        }
    }
}
