﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SQLSingleton
{
    /// <summary>
    /// Interaction logic for SqlConnectionControl.xaml
    /// </summary>
    public partial class SqlConnectionControl : UserControl
    {
        private SqlConnectionStringBuilder _connectionStringBuilder;

        public string HostName => _connectionStringBuilder == null ? null : _connectionStringBuilder.DataSource;
        public string DatabaseName => _connectionStringBuilder == null ? null : _connectionStringBuilder.InitialCatalog;
        public bool IntegratedSecurity => _connectionStringBuilder != null && _connectionStringBuilder.IntegratedSecurity;
        public string UserName => _connectionStringBuilder == null ? null : _connectionStringBuilder.UserID;
        public string Password => _connectionStringBuilder == null ? null : _connectionStringBuilder.Password;

        public string ConnectionString => _connectionStringBuilder == null ? null : _connectionStringBuilder.ConnectionString;

        public SqlConnectionControl()
        {
            InitializeComponent();
        }

        public SqlConnectionControl(bool allowIntegratedSecurity)
        {
            InitializeComponent();
            AllowIntegratedSecurity(allowIntegratedSecurity);
        }

        public SqlConnectionControl(string hostName, string dataBaseName, string userName = "sa", bool allowIntegratedSecurity = true)
        {
            InitializeComponent();

            CbHostName.Text = hostName;
            try { CbDataBase.Items.Clear(); }
            catch { CbDataBase.ItemsSource = null; }
            CbDataBase.Items.Add(dataBaseName);
            CbDataBase.SelectedIndex = 0;
            TbUserName.Text = userName;

            AllowIntegratedSecurity(allowIntegratedSecurity);
        }
        /*
        public SqlConnectionControl(RegistryData registryData, bool allowIntegratedSecurity)
        {
            InitializeComponent();

            CbHostName.Text = registryData.Hostname;
            try { CbDataBase.Items.Clear(); }
            catch { CbDataBase.ItemsSource = null; }
            CbDataBase.Items.Add(registryData.Database);
            CbDataBase.SelectedIndex = 0;
            TbUserName.Text = registryData.Username;
            /*
            if (!string.IsNullOrWhiteSpace(registryData.Password))
            {
                RbSqlAuth.IsChecked = true;
                TbPassword.Password = registryData.Password;
            }
            *//*
            AllowIntegratedSecurity(allowIntegratedSecurity);
        }

        public SqlConnectionControl(List<RegistryData> registryDataList, bool allowIntegratedSecurity)
        {
            InitializeComponent();

            _registryDataList = registryDataList;

            if (registryDataList != null && registryDataList.Count > 0)
            {
                CbHostName.Text = registryDataList[0].Hostname;
                try { CbDataBase.Items.Clear(); }
                catch { CbDataBase.ItemsSource = null; }
                CbDataBase.Items.Add(registryDataList[0].Database);
                CbDataBase.SelectedIndex = 0;
                TbUserName.Text = registryDataList[0].Username;
                /*
                if (!string.IsNullOrWhiteSpace(registryDataList[0].Password))
                {
                    RbSqlAuth.IsChecked = true;
                    TbPassword.Password = registryDataList[0].Password;
                }
                *//*
            }

            AllowIntegratedSecurity(allowIntegratedSecurity);
}
    */

        private void AllowIntegratedSecurity(bool value)
        {
            if (value)
                return;

            RbSqlAuth.IsChecked = true;
            RbWinAuth.IsEnabled = false;
        }

        private void ToggleButton_OnCheckedChanged(object sender, RoutedEventArgs e)
        {
            if (GpSqlAuth == null)
                return;

            GpSqlAuth.IsEnabled = !(RbWinAuth.IsChecked ?? false);
        }

        private void BtnAbort_OnClick(object sender, RoutedEventArgs e)
        {
            OnCalledClose(this, new ConnectionControlCalledCloseEventArgs(false));
        }

        private void BtnOk_OnClick(object sender, RoutedEventArgs e)
        {
            Cursor = Cursors.Wait;
            if (SqlConnectionForm.TestConnection(BuildConnectionString()))
            {
                OnCalledClose(this, new ConnectionControlCalledCloseEventArgs(true));
            }
            else
            {
                MessageBox.Show("Es konnte keine Verbindung hergestellt werden!");
                Cursor = Cursors.Arrow;
            }
        }

        internal event ConnectionControlCalledCloseEventHandler OnCalledClose = delegate { };
        internal delegate void ConnectionControlCalledCloseEventHandler(object sender, ConnectionControlCalledCloseEventArgs args);

        #region SQL Server instance
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            _searchTask = new Task(SearchInstances);
            _searchTask.Start();
        }

        bool _triedToOpen = false;
        readonly List<string> _sqlInstances = new List<string>();
        private Task _searchTask;
        public void SearchInstances()
        {
            if (_sqlInstances == null || _sqlInstances.Count != 0)
                return;

            var servers = SqlDataSourceEnumerator.Instance;
            var serversTable = servers.GetDataSources();
            if (serversTable.Rows.Count > 0)
                foreach (DataRow row in serversTable.Rows)
                {
                    var serverName = row[0].ToString();

                    try
                    {
                        if (row[1].ToString() != "")
                            serverName += "\\" + row[1].ToString();
                    }
                    catch { }

                    _sqlInstances.Add(serverName);
                }
            else
                _sqlInstances.Add(".");

            InvokeUI(() => {
                ProgressRing.Visibility = Visibility.Collapsed;
                if (_triedToOpen)
                {
                    CbHostName.IsDropDownOpen = true;
                    _triedToOpen = false;
                }
            });
        }

        private void InvokeUI(Action a)
        {
            Dispatcher.BeginInvoke(new System.Windows.Forms.MethodInvoker(a));
        }

        private void CbHostName_OnContextMenuOpening(object sender, EventArgs eventArgs)
        {
            if (_searchTask != null && !_searchTask.IsCompleted)
            {
                ProgressRing.Visibility = Visibility.Visible;
                _triedToOpen = true;
                CbHostName.IsDropDownOpen = false;
                return;
            }

            if (_sqlInstances == null || _sqlInstances.Count <= 0)
                return;

            CbHostName.ItemsSource = _sqlInstances;

            if (_sqlInstances.Count == 1)
                CbHostName.Text = _sqlInstances[0];
        }
        #endregion
        #region DataBase name
        private string BuildConnectionString()
        {
            _connectionStringBuilder = new SqlConnectionStringBuilder();

            if (CbHostName.Text.Length == 0)
                return null;

            _connectionStringBuilder.DataSource = CbHostName.Text;

            if (RbSqlAuth.IsChecked ?? false)
            {
                _connectionStringBuilder.UserID = TbUserName.Text;
                _connectionStringBuilder.Password = TbPassword.Password;
            }
            else
                _connectionStringBuilder.IntegratedSecurity = true;

            if (_dropDownOpening)
                _connectionStringBuilder.ConnectTimeout = 3;
            else if (CbDataBase.Text.Length > 0)
                _connectionStringBuilder.InitialCatalog = CbDataBase.Text;

            return _connectionStringBuilder.ConnectionString;
        }

        bool _dropDownOpening = false;
        private void CbDataBase_OnContextMenuOpening(object sender, EventArgs eventArgs)
        {
            _dropDownOpening = true;
            ProgressRing2.Visibility = Visibility.Visible;
            Cursor = Cursors.Wait;

            try
            {
                CbDataBase.Items.Clear();
            }
            catch { }

            CbDataBase.ItemsSource = null;

            string con = BuildConnectionString();

            if (!String.IsNullOrEmpty(con))
            {
                using (SqlConnection sqlCon = new SqlConnection(con))
                {
                    try
                    {
                        sqlCon.Open();

                        //SELECT name FROM sys.databases WHERE state = 0
                        SqlDataAdapter sda = new SqlDataAdapter("SELECT name FROM sys.databases WHERE state = 0", sqlCon);
                        System.Data.DataTable dt = new DataTable();
                        sda.Fill(dt);

                        List<string> dbNames = new List<string>();

                        foreach (DataRow row in dt.Rows)
                            dbNames.Add(row[0].ToString());

                        //CbDataBase.ItemsSource = dbNames.OrderBy(x => x);
                        dbNames.Sort();
                        CbDataBase.ItemsSource = dbNames;
                    }
                    catch (SqlException ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        sqlCon.Close();
                    }
                }
            }

            _dropDownOpening = false;
            ProgressRing2.Visibility = Visibility.Collapsed;
            Cursor = Cursors.Arrow;
        }
        #endregion
    }

    internal class ConnectionControlCalledCloseEventArgs
    {
        public bool Result { get; private set; }

        public ConnectionControlCalledCloseEventArgs(bool result) => Result = result;
    }
}
